from django.contrib import admin
from .models import ClassYear, Friend, Work

admin.site.register(ClassYear)
admin.site.register(Friend)
admin.site.register(Work)
