from django.shortcuts import render
from .models import *
from .forms import FriendForm
from django.shortcuts import redirect
from django.contrib import messages


def index(request):
    return render(request, 'index.html', {
        'title': 'Home',
        'bgColor': 'white',
        'navState': ['active', '', ''],
    })


def works(request):
    worksList = Work.objects.all()
    worksListOne, worksListTwo = splitList(worksList)

    return render(request, 'works.html', {
        'title': 'Works',
        'bgColor': 'black',
        'navState': ['', 'active', ''],
        'worksListOne': worksListOne,
        'worksListTwo': worksListTwo,
    })


def friends(request):
    friendsList = Friend.objects.all()
    
    return render(request, 'friends.html', {
        'title': 'Friends',
        'bgColor': 'white',
        'navState': ['', '', 'active'],
        'friendsList': friendsList,
    })


def form(request):
    if request.method == "POST":
        form = FriendForm(request.POST)

        if form.is_valid():
            form.save()
            messages.success(request, "Your request will be evaluated by Figo. If accepted, your name will be displayed on the friend's list.")

    form = FriendForm()
    return render(request, 'form.html', {
        'title': 'Friend Form',
        'bgColor': 'white',
        'navState': ['', '', ''],
        'form': form
    })


def splitList(a_list):
    half = len(a_list)//2
    return a_list[:half], a_list[half:]
