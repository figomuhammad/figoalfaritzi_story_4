from django.db import models


class ClassYear(models.Model):
    year = models.IntegerField()

    def __str__(self):
        return str(self.year)


class Friend(models.Model):
    firstName = models.CharField(max_length=30)
    lastName = models.CharField(max_length=30)
    classYear = models.ForeignKey(ClassYear, on_delete=models.CASCADE)
    hobby = models.CharField(max_length=255)
    favoriteFood = models.CharField(max_length=255)
    favoriteDrink = models.CharField(max_length=255)
    isValidated = models.BooleanField(default=False)

    def __str__(self):
        return self.firstName + " " + self.lastName


class Work(models.Model):
    imageName = models.CharField(max_length=255)

    def __str__(self):
        return self.imageName
