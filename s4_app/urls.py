from django.urls import include, path
from .views import works, index, friends, form

urlpatterns = [
    path('', index, name='index'),
    path('works', works, name='works'),
    path('home', index, name='home'),
    path('friends', friends, name='friends'),
    path('form', form, name='form'),
]
