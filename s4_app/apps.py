from django.apps import AppConfig


class S4AppConfig(AppConfig):
    name = 's4_app'
