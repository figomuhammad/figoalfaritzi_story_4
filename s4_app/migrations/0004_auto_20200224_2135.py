# Generated by Django 3.0.2 on 2020-02-24 14:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('s4_app', '0003_work'),
    ]

    operations = [
        migrations.RenameField(
            model_name='friend',
            old_name='year',
            new_name='classYear',
        ),
    ]
