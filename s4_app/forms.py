from django import forms
from .models import Friend


class FriendForm(forms.ModelForm):

    class Meta:
        model = Friend
        fields = ('firstName', 'lastName', 'classYear', 'hobby', 'favoriteFood', 'favoriteDrink')
        widgets = {
            'firstName': forms.TextInput(attrs={'class': 'form-control'}),
            'lastName': forms.TextInput(attrs={'class': 'form-control'}),
            'classYear': forms.Select(attrs={'class': 'form-control'}),
            'hobby': forms.TextInput(attrs={'class': 'form-control'}),
            'favoriteFood': forms.TextInput(attrs={'class': 'form-control'}),
            'favoriteDrink': forms.TextInput(attrs={'class': 'form-control'}),
        }